from esc import CR, LF
import esccmd
import escio
from escutil import AssertEQ
from escutil import AssertScreenCharsInRectEqual
from escutil import GetCharInCell
from escutil import GetCursorPosition
from escutil import GetScreenSize
from escutil import Rect
from escutil import knownBug
from escutil import vtLevel
from esctypes import Point

class FillRectangleTests(object):

  @classmethod
  def data(cls):
    return ["abcdefgh",
            "ijklmnop",
            "qrstuvwx",
            "yz012345",
            "ABCDEFGH",
            "IJKLMNOP",
            "QRSTUVWX",
            "YZ6789!@"]

  def prepare(self):
    esccmd.CUP(Point(1, 1))
    for line in self.data():
      escio.Write(line + CR + LF)

  def fill(self, top=None, left=None, bottom=None, right=None, character=None):
    """Subclasses should override this to do the appropriate fill action."""
    pass

  @classmethod
  def characters(cls, point, count):
    """Returns the filled characters starting at point, and count of them."""
    return "!" * count

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="wezterm", reason="Not implemented")
  def fillRectangle_basic(self):
    self.prepare()
    self.fill(top=5,
              left=5,
              bottom=7,
              right=7)
    AssertScreenCharsInRectEqual(Rect(1, 1, 8, 8),
                                 ["abcdefgh",
                                  "ijklmnop",
                                  "qrstuvwx",
                                  "yz012345",
                                  "ABCD" + self.characters(Point(5, 5), 3) + "H",
                                  "IJKL" + self.characters(Point(5, 6), 3) + "P",
                                  "QRST" + self.characters(Point(5, 7), 3) + "X",
                                  "YZ6789!@"])

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  def fillRectangle_invalidRectDoesNothing(self):
    self.prepare()
    self.fill(top=5,
              left=5,
              bottom=4,
              right=4)
    AssertScreenCharsInRectEqual(Rect(1, 1, 8, 8),
                                 ["abcdefgh",
                                  "ijklmnop",
                                  "qrstuvwx",
                                  "yz012345",
                                  "ABCDEFGH",
                                  "IJKLMNOP",
                                  "QRSTUVWX",
                                  "YZ6789!@"])

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="wezterm", reason="Not implemented")
  def fillRectangle_defaultArgs(self):
    """Write a value at each corner, run fill with no args, and verify the
    corners have all been replaced with self.character."""
    size = GetScreenSize()
    points = [Point(1, 1),
              Point(size.width(), 1),
              Point(size.width(), size.height()),
              Point(1, size.height())]
    n = 1
    for point in points:
      esccmd.CUP(point)
      escio.Write(str(n))
      n += 1

    self.fill()

    for point in points:
      AssertScreenCharsInRectEqual(
          Rect(point.x(), point.y(), point.x(), point.y()),
          [self.characters(point, 1)])

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="wezterm", reason="Not implemented")
  def fillRectangle_defaultCharacterArgIsSpace(self, char_value):
    """Write a value at each corner, run fill with specified Pch, and verify the
    corners have all been replaced with SPACE."""
    size = GetScreenSize()
    points = [Point(1, 1),
              Point(size.width(), 1),
              Point(size.width(), size.height()),
              Point(1, size.height())]
    n = 1
    for point in points:
      esccmd.CUP(point)
      escio.Write(str(n))
      n += 1

    self.fill(character=str(char_value) if char_value is not None else None)

    for point in points:
      AssertScreenCharsInRectEqual(
          Rect(point.x(), point.y(), point.x(), point.y()),
          [" " * 1])

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="wezterm", reason="Not implemented")
  def fillRectangle_respectsOriginMode(self):
    self.prepare()

    # Set margins starting at 2 and 2
    esccmd.DECSET(esccmd.DECLRMM)
    esccmd.DECSLRM(2, 9)
    esccmd.DECSTBM(2, 9)

    # Turn on origin mode
    esccmd.DECSET(esccmd.DECOM)

    # Fill from 1,1 to 3,3 - with origin mode, that's 2,2 to 4,4
    self.fill(top=1,
              left=1,
              bottom=3,
              right=3)

    # Turn off margins and origin mode
    esccmd.DECRESET(esccmd.DECLRMM)
    esccmd.DECSTBM()
    esccmd.DECRESET(esccmd.DECOM)

    # See what happened.
    AssertScreenCharsInRectEqual(Rect(1, 1, 8, 8),
                                 ["abcdefgh",
                                  "i" + self.characters(Point(2, 2), 3) + "mnop",
                                  "q" + self.characters(Point(2, 3), 3) + "uvwx",
                                  "y" + self.characters(Point(2, 4), 3) + "2345",
                                  "ABCDEFGH",
                                  "IJKLMNOP",
                                  "QRSTUVWX",
                                  "YZ6789!@"])

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="wezterm", reason="Not implemented")
  def fillRectangle_overlyLargeSourceClippedToScreenSize(self):
    size = GetScreenSize()

    # Put ab, cX in the bottom right
    esccmd.CUP(Point(size.width() - 1, size.height() - 1))
    escio.Write("ab")
    esccmd.CUP(Point(size.width() - 1, size.height()))
    escio.Write("cd")

    # Fill a 2x2 block starting at the d.
    self.fill(top=size.height(),
              left=size.width(),
              bottom=size.height() + 10,
              right=size.width() + 10)
    AssertScreenCharsInRectEqual(Rect(size.width() - 1,
                                      size.height() - 1,
                                      size.width(),
                                      size.height()),
                                 ["ab",
                                  "c" + self.characters(Point(size.width(), size.height()), 1)])

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented", noop=True)
  @knownBug(terminal="wezterm", reason="Not implemented", noop=True)
  def fillRectangle_cursorDoesNotMove(self):
    # Make sure something is on screen (so the test is more deterministic)
    self.prepare()

    # Place the cursor
    position = Point(3, 4)
    esccmd.CUP(position)

    # Fill a block
    self.fill(top=2,
              left=2,
              bottom=4,
              right=4)

    # Make sure the cursor is where we left it.
    AssertEQ(GetCursorPosition(), position)

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="wezterm", reason="Not implemented")
  def fillRectangle_ignoresMargins(self):
    self.prepare()

    # Set margins
    esccmd.DECSET(esccmd.DECLRMM)
    esccmd.DECSLRM(3, 6)
    esccmd.DECSTBM(3, 6)

    # Fill!
    self.fill(top=5,
              left=5,
              bottom=7,
              right=7)

    # Remove margins
    esccmd.DECRESET(esccmd.DECLRMM)
    esccmd.DECSTBM()

    # Did it ignore the margins?
    AssertScreenCharsInRectEqual(Rect(1, 1, 8, 8),
                                 ["abcdefgh",
                                  "ijklmnop",
                                  "qrstuvwx",
                                  "yz012345",
                                  "ABCD" + self.characters(Point(5, 5), 3) + "H",
                                  "IJKL" + self.characters(Point(5, 6), 3) + "P",
                                  "QRST" + self.characters(Point(5, 7), 3) + "X",
                                  "YZ6789!@"])

  @vtLevel(4)
  @knownBug(terminal="iTerm2", reason="Not implemented")
  @knownBug(terminal="wezterm", reason="Not implemented")
  def fillRectangle_outsideMarginsInOriginMode(self):
    """Check that a fill completely outside the margins is clamped correctly."""
    self.prepare()

    # Set margins
    esccmd.DECSET(esccmd.DECLRMM)
    esccmd.DECSLRM(2, 5)
    esccmd.DECSTBM(2, 5)

    # Turn on origin mode
    esccmd.DECSET(esccmd.DECOM)

    # Fill from 5,5 to 8,8 - with origin mode, that's 6,6 to 9,9
    self.fill(top=5,
              left=5,
              bottom=8,
              right=8)

    # Turn off margins and origin mode
    esccmd.DECRESET(esccmd.DECLRMM)
    esccmd.DECSTBM()
    esccmd.DECRESET(esccmd.DECOM)

    # See what happened.
    AssertScreenCharsInRectEqual(Rect(1, 1, 8, 8),
                                 ["abcdefgh",
                                  "ijklmnop",
                                  "qrstuvwx",
                                  "yz012345",
                                  "ABCD" + self.characters(Point(5, 5), 1) + "FGH",
                                  "IJKLMNOP",
                                  "QRSTUVWX",
                                  "YZ6789!@"])

  @vtLevel(4)
  def fillRectangle_unicodeCharacter(self, character, utf16=False):
    def charvalue(c, utf):
      if c < 0x10000:
        return c
      else:
        if utf:
          c -= 0x10000
          return [((c >> 10) & 0x3ff) + 0xd800, (c & 0x3ff) + 0xdc00]
        else:
          return [c >> 16, c & 0xffff]

    self.prepare()
    self.fill(top=5,
              left=5,
              bottom=7,
              right=7,
              character=charvalue(character, utf16))
    for point in Rect(5, 5, 7, 7).points():
      # The DECRQCRA checksum only delivers the lower 16 bits
      # of thecharacter value
      AssertEQ(GetCharInCell(point), character & 0xffff)

  @vtLevel(4)
  def fillRectangle_invokedCharsetCharacter(self, slot, value, translated):
    self.prepare()

    # Designate and invoke the DEC special graphics charset
    esccmd.GnDm(charset='0', slot=slot)
    if slot == '(':
      esccmd.LS0()
    elif slot == ')':
      esccmd.LS1R()

    self.fill(top=5,
              left=5,
              bottom=7,
              right=7,
              character=value)
    esccmd.GnDm()

    for point in Rect(5, 5, 7, 7).points():
      AssertEQ(GetCharInCell(point), translated)

  @vtLevel(4)
  def fillRectangle_characterFromBMP(self):
    """Fills with a non-7-bit BMP character."""
    self.fillRectangle_unicodeCharacter(0x2592) # U+2592 MEDIUM SHADE

  @vtLevel(4)
  def fillRectangle_characterFromSMP(self):
    """Fills with a SMP character."""
    self.fillRectangle_unicodeCharacter(0x1fb95, False) # U+1FB95 CHECKER BOARD FILL

  @vtLevel(4)
  def fillRectangle_characterFromSMPAsSurrogatesPair(self):
    """Fills with a SMP character."""
    self.fillRectangle_unicodeCharacter(0x1fb95, True) # U+1FB95 CHECKER BOARD FILL

  @vtLevel(4)
  def fillRectangle_characterFromGL(self):
    """Fills with a GL character."""
    self.fillRectangle_invokedCharsetCharacter(slot='(', value=0x60, translated=0x25c6)

  @vtLevel(4)
  def fillRectangle_characterFromGR(self):
    """Fills with a GR character."""
    self.fillRectangle_invokedCharsetCharacter(slot=')', value=0xd1, translated=0x2592)
