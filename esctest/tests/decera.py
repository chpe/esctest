import tests.decrectops

import esc
import esccmd
from escutil import knownBug, vtFirmwareVersion

from escutil import knownBug

class DECERATests(tests.decrectops.FillRectangleTests):
  def fill(self, top=None, left=None, bottom=None, right=None):
    esccmd.DECERA(top, left, bottom, right)

  def characters(self, point, count):
    return esc.blank() * count

  @knownBug(terminal="iTerm2beta", reason="Erase fills rather than clears.")
  @knownBug(terminal="konsole", reason="DECERA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECERA_basic(self):
    self.fillRectangle_basic()

  @knownBug(terminal="konsole", reason="DECERA not implemented.", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECERA_invalidRectDoesNothing(self):
    self.fillRectangle_invalidRectDoesNothing()

  @knownBug(terminal="iTerm2beta", reason="Erase fills rather than clears.")
  @knownBug(terminal="konsole", reason="DECERA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECERA_defaultArgs(self):
    self.fillRectangle_defaultArgs()

  @knownBug(terminal="iTerm2beta", reason="Erase fills rather than clears.")
  @knownBug(terminal="konsole", reason="DECERA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECERA_respectsOriginMode(self):
    self.fillRectangle_respectsOriginMode()

  @knownBug(terminal="iTerm2beta", reason="Erase fills rather than clears.")
  @knownBug(terminal="konsole", reason="DECERA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECERA_overlyLargeSourceClippedToScreenSize(self):
    self.fillRectangle_overlyLargeSourceClippedToScreenSize()

  @knownBug(terminal="konsole", reason="DECERA not implemented.", noop=True)
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECERA_cursorDoesNotMove(self):
    self.fillRectangle_cursorDoesNotMove()

  @knownBug(terminal="iTerm2beta", reason="Erase fills rather than clears.")
  @knownBug(terminal="konsole", reason="DECERA not implemented.")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECERA_ignoresMargins(self):
    self.fillRectangle_ignoresMargins()

  @knownBug(terminal="konsole", reason="DECFRA not implemented.")
  @knownBug(terminal="xterm", reason="Unknown")
  @vtFirmwareVersion(terminal="vte", minimum=7700)
  def test_DECERA_outsideMarginsInOriginMode(self):
    self.fillRectangle_outsideMarginsInOriginMode()
