"""Tests for VTE termprops
"""

import esccmd
import escio
from escutil import AssertEQ, AssertTrue, onlyIn, vtFirmwareVersion
from base64 import b64encode
from uuid import UUID

class VteTermpropTests(object):

  def writeTermpropOSC(self, args, hasQuery=False):
    params=[666]
    params.extend(args)
    escio.WriteOSC(params, requestsReport=hasQuery)

  def doSetTermprops(self, args, hasQuery=False):
    params = []
    for name, value in args:
      if value is None:
        params += [ name ]
      else:
        params += [ name + "=" + str(value) ]

    return self.writeTermpropOSC(params, hasQuery)

  def doSetTermprop(self, name, value, hasQuery=False):
    return self.doSetTermprops([[name, value]])

  def doQueryTermprop(self, name):
    self.writeTermpropOSC([name + "?"], True)
    return escio.ReadOSC("666;").split(";")

  def doSetAndAssertTermpropValue(self, name, value, expected_value):
    """Checks that the termprop has the requisite value after setting it to that value."""
    self.doSetTermprop(name, value)

    reply = self.doQueryTermprop(name)
    AssertEQ(len(reply), 1)
    if expected_value is None:
      AssertEQ(reply[0], name)
    else:
      params = reply[0].split("=", 1)
      AssertEQ(len(params), 2)
      AssertEQ(params[0], name)
      AssertEQ(expected_value, type(expected_value)(params[1]))

  def doResetAndAssertTermpropValue(self, name):
    """Checks that the termprop has no value after resetting it."""
    self.doSetAndAssertTermpropValue(name, None, None)

  def doSetAndAssertTermpropValues(self, name, arr):
    for value, expected_value in arr:
      self.doSetAndAssertTermpropValue(name, value, expected_value)

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_Int(self):
    self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.int",
                                      [["0", 0],
                                       ["1", 1],
                                       ["9223372036854775807", 9223372036854775807], # max
                                       ["-1", -1],
                                       ["-9223372036854775808", -9223372036854775808], # min
                                       ["9223372036854775808", None], # too large
                                       ["-9223372036854775809", None], # too small
                                       ["0xff", None], # hex not allowed
                                       ["-", None], # lone minus sign
                                       ["foo", None]]) # not an integer

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_UInt(self):
    self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.uint",
                                      [["0", 0],
                                       ["1", 1],
                                       ["18446744073709551614", 18446744073709551614], # max - 1
                                       ["18446744073709551615", 18446744073709551615], # max
                                       ["18446744073709551616", None], # too large
                                       ["-1", None], # negative
                                       ["0xff", None], # hex not allowed
                                       ["foo", None]]) # not an integer

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_Double(self):
    self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.double",
                                      [["0", 0.0],
                                       ["0.1", 0.1],
                                       ["1.0", 1.0],
                                       ["2.0E8", 2.0E8],
                                       [" 1.0", None], # leading whitespace
                                       ["1.0 ", None], # trailing whitespace
                                       ["0x12345678", None], # hex not allowed
                                       ["Inf", None], # no infinities
                                       ["-Inf", None], # no infinities
                                       ["NaN", None]]) # no NaNs

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_RGB(self):
    self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.rgb",
                                      [["#123456", "#123456"],
                                       ["#abcdef", "#ABCDEF"],
                                       ["#ABCDEF", "#ABCDEF"],
                                       ["#000", "#000000"],
                                       ["#0000", "#000000"],
                                       ["#0", None],
                                       ["#00", None],
                                       ["#00000", None],
                                       ["000", None]])

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_RGBA(self):
    self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.rgba",
                                      [["#123456", "#123456FF"],
                                       ["#abcdef", "#ABCDEFFF"],
                                       ["#ABCDEF", "#ABCDEFFF"],
                                       ["#12345678", "#12345678"],
                                       ["#abcdef01", "#ABCDEF01"],
                                       ["#ABCDEF76", "#ABCDEF76"],
                                       ["#000", "#000000FF"],
                                       ["#0000", "#00000000"],
                                       ["#0", None],
                                       ["#00", None],
                                       ["#00000", None],
                                       ["000", None]])

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_String(self):
    self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.string",
                                      [["", ""],
                                       ["abc", "abc"],
                                       ["a" * 1024, "a" * 1024],
                                       ["a" * 1025, None], # max length exceeded
                                       ["a\\sb\\nc\\\\d", "a\\sb\\nc\\\\d"], # escapes
                                       ["a;b", "a"], # unescaped semicolon
                                       ["a\nb", "ab"], # controls are ignored in OSC
                                       ["a\\", None], # invalid escape
                                       ["a\\a", None]]) # invalid escape

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_Data(self):

    def b64(ba):
      return str(b64encode(ba), "utf-8")

    self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.data",
                                      [["", ""],
                                       ["YQ==", "YQ=="], # "a"
                                       ["YWE=", "YWE="], # "aa"
                                       ["YWFh", "YWFh"], # "aaa"
                                       ["AA==", "AA=="], # "\0"
                                       ["YQBi", "YQBi"], # "a\0b"
                                       ["gMH/YWJj", "gMH/YWJj"], # "\x80\xc1\xff""abc" (not UTF-8)
                                       [b64(b"a" * 2048), b64(b"a" * 2048)], # max length
                                       [b64(b"a" * 2049), None], # max length exceeded
                                       ["YQ=", None], # incomplete
                                       ["YQ", None], # incomplete
                                       ["Y", None]]) # incomplete

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_UUID(self):

    u = UUID(fields=(0x49ec5248, 0x2d9a, 0x493f, 0x99, 0xfa, 0x9e1cfb95b430))
    self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.uuid",
                                      [["49ec5248-2d9a-493f-99fa-9e1cfb95b430", u],
                                       ["{49ec5248-2d9a-493f-99fa-9e1cfb95b430}", u],
                                       ["urn:uuid:49ec5248-2d9a-493f-99fa-9e1cfb95b430", u],
                                       ["49ec5248-2d9a-493f-99fa-9e1cfb95b43", None], # wrong length
                                       ["{49ec5248-2d9a-493f-99fa-9e1cfb95b430", None], # missing brace
                                       ["urn:49ec5248-2d9a-493f-99fa-9e1cfb95b430", None], # missing "uuid:"
                                       ["{urn:uuid:49ec5248-2d9a-493f-99fa-9e1cfb95b430}", None]]) # cannot use braces and urn together

  #@onlyIn(terminal="vte", reason="vte-only feature")
  #@vtFirmwareVersion("vte", 7700)
  #def test_VteTermprop_URI(self):
  #  self.doSetAndAssertTermpropValues("vte.ext.vteapp.test.uri",
  #                                    [["https://www.freedesktop.org/", "https://www.freedesktop.org/"],
  #                                     ["file:///usr/bin/cat", "file:///usr/bin/cat"],
  #                                     ["data:text/plain;base64,QQo=", None], # data: not allowed, unescaped semicolon
  #                                     ["data:text/plain%3BQbase64,Qo="]]) # data: not allowed

  @onlyIn(terminal="vte", reason="vte-only feature")
  @vtFirmwareVersion("vte", 7700)
  def test_VteTermprop_ResetAndAssertNoValue(self):
    """Checks that a termprop has no value after reset."""
    self.doSetAndAssertTermpropValue("vte.ext.vteapp.test.string", "foobar", "foobar")
    self.doResetAndAssertTermpropValue("vte.ext.vteapp.test.string")
